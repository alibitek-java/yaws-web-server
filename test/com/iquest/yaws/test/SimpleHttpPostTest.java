package com.iquest.yaws.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

/**
 * http://www.vogella.com/articles/ApacheHttpClient/article.html
 *
 * Send a POST request to the URL "http://vogellac2dm.appspot.com/register"
 * and include a parameter "registrationid" which an random value.
 * 
 * @author Alex Butum
 *
 */
public class SimpleHttpPostTest {
	
	@Test
    public void testHttpPost() {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost("http://vogellac2dm.appspot.com/register");

        List<NameValuePair> nameValuePairList = new ArrayList<>(1);
        nameValuePairList.add(new BasicNameValuePair("registrationid", "123456789"));
        try {
            post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
            HttpResponse response = client.execute(post);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
