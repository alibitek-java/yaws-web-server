package com.iquest.yaws.http;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * The first line of a Response message is the Status-Line
 * <p/>The status line consists of the following:
 * <p/>Status-Line = HTTP-Version SP Status-Code SP Reason-Phrase CRLF
 * <p/>This enum represents the Status-Code + Reason-Phrase of the Status-Line
 *
 * @author Alex Butum
 * @see <a href="http://tools.ietf.org/html/rfc2616.html#section-6.1.1">RFC 2616 Section 6.1.1</a>
 */
public enum HttpStatusCode {

    /* Status Codes and Reason Phrases*/

    // 1xx: Informational - Request received, continuing process
    CONTINUE(100, "Continue"),
    SWITCHING_PROTOCOLS(101, "Switching Protocols"),

    // 2xx: Success - The action was successfully received, understood, and accepted
    OK(200, "OK"),
    CREATED(201, "Created"),
    ACCEPTED(202, "Accepted"),
    NON_AUTHORITATIVE_INFORMATION(203, "Non-Authoritative Information"),
    NO_CONTENT(204, "No Content"),
    RESET_CONTENT(205, "Reset Content"),
    PARTIAL_CONTENT(206, "Partial Content"),

    // 3xx: Redirection - Further action must be taken in order to complete the request
    MULTIPLE_CHOICES(300, "Multiple Choices"),
    MOVED_PERMANENTLY(301, "Moved Permanently"),
    FOUND(302, "Found"),
    SEE_OTHER(303, "See Other"),
    NOT_MODIFIED(304, "Not Modified"),
    USE_PROXY(305, "Use Proxy"),
    SWITCH_PROXY(306, "Switch Proxy"),
    TEMPORARY_REDIRECT(307, "Temporary Redirect "),

    // 4xx: Client Error - The request contains bad syntax or cannot be fulfilled
    BAD_REQUEST(400, "Bad Request"),
    UNAUTHORIZED(401, "Unauthorized"),
    PAYMENT_REQUIRED(402, "Payment Required"),
    FORBIDDEN(403, "Forbidden"),
    NOT_FOUND(404, "Not Found"),
    METHOD_NOT_ALLOWED(405, "Method Not Allowed"),
    NOT_ACCEPTABLE(406, "Not Acceptable"),
    PROXY_AUTHENTICATION_REQUIRED(407, "Proxy Authentication Required"),
    REQUEST_TIMEOUT(408, "Request Timeout"),
    CONFLICT(409, "Conflict"),
    GONE(410, "Gone"),
    LENGTH_REQUIRED(411, "Length Required"),
    PRECONDITION_FAILED(412, "Precondition Failed"),
    REQUEST_ENTITY_TOO_LARGE(413, "Request Entity Too Large"),
    REQUEST_URI_TOO_LONG(414, "Request-URI Too Long"),
    UNSUPPORTED_MEDIA_TYPE(415, "Unsupported Media Type"),
    REQUESTED_RANGE_NOT_SATISFIABLE(416, "Requested Range Not Satisfiable"),
    EXPECTATION_FAILED(417, "Expectation Failed"),

    // 5xx: Server Error - The server failed to fulfill an apparently valid request
    INTERNAL_SERVER_ERROR(500, "Internal Server Error"),
    NOT_IMPLEMENTED(501, "Not Implemented"),
    BAD_GATEWAY(502, "Bad Gateway"),
    SERVICE_UNAVAILABLE(503, "Service Unavailable"),
    GATEWAY_TIMEOUT(504, "Gateway Timeout"),
    HTTP_VERSION_NOT_SUPPORTED(505, "HTTP Version Not Supported");

    // The Status-Code is intended for use by automata and the Reason-Phrase is intended for the human user.
    private final int statusCode;

    // The Reason-Phrase is intended to give a short textual description of the Status-Code
    private final String reasonPhrase;

    private static final Map<Integer, HttpStatusCode> codeLookupTable = new HashMap<>();

    static {
        for (HttpStatusCode s : EnumSet.allOf(HttpStatusCode.class)) {
            codeLookupTable.put(s.getStatusCode(), s);
        }
    }

    private HttpStatusCode(int statusCode, String reasonPhrase) {
        this.statusCode = statusCode;
        this.reasonPhrase = reasonPhrase;
    }

    /**
     * Returns the numerical statusCode.
     *
     * @return status code
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Returns the reason phrase
     *
     * @return reason phrase
     */
    public String getReasonPhrase() {
        return reasonPhrase;
    }

    /**
     * Gets the {@link yaws.http.HttpStatusCode} type by the statusCode number.
     *
     * @param code numerical statusCode representation (i.e. 404)
     * @return assosciated status statusCode
     */
    public static HttpStatusCode getStatusCode(int code) {
        return codeLookupTable.get(code);
    }
}
