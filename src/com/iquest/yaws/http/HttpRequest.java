package com.iquest.yaws.http;

import java.io.InputStream;
import java.util.Map;

/**
 * 
 * Handle HTTP requests
 *
 */
public class HttpRequest extends HttpMessage {

    private HttpMethod httpMethod;
    private String requestUri;
    private Map<String, String> queryParams;
    private InputStream inputStream;
    private String requestHeader;

    public HttpRequest(InputStream in) {

    }

    /**
     * Returns the HTTP method fo the request
     *
     * @return the HTTP method of this request.
     * @see <a href="http://tools.ietf.org/html/rfc2616.html#section-5.1.1">RFC 2616 Section 5.1.1</a>
     */
    public HttpMethod getHttpMethod() {
        return httpMethod;
    }

    /**
     * Returns the request URI of this request.
     *
     * @return the request URI as string
     * @see <a href="http://tools.ietf.org/html/rfc2616.html#section-5.1.2">RFC 2616 Section 5.1.2</a>
     */
    public String getRequestUri() {
        return requestUri;
    }

    public Map<String, String> getQueryParams() {
        return queryParams;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public String getRequestHeader() {
        return requestHeader;
    }

    private void readInputStream() {

    }

	public void setHttpMethod(HttpMethod httpMethod) {
		this.httpMethod = httpMethod;
	}

	public void setRequestUri(String requestUri) {
		this.requestUri = requestUri;
	}

	public void setQueryParams(Map<String, String> queryParams) {
		this.queryParams = queryParams;
	}

	public void setRequestHeader(String requestHeader) {
		this.requestHeader = requestHeader;
	}   
}
