package com.iquest.yaws.http;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Available HTTP versions.
 *
 * @author Alex Butum
 * @see <a href="http://tools.ietf.org/html/rfc2616.html#section-3.1">RFC 2616 Section 3.1</a>
 */
public enum HttpVersion {
    /**
     * HTTP/1.0
     */
    VERSION_1_0(1, 0),

    /**
     * HTTP/1.1
     */
    VERSION_1_1(1, 1);

    private final int major;

    private final int minor;

    private HttpVersion(int major, int minor) {
        this.major = major;
        this.minor = minor;
    }

    @Override
    public String toString() {
        return Http.HTTP + Http.PROTOCOL_DELIMITER + major + "." + minor;
    }

    /**
     * Extracts the HTTP version from the status line.
     *
     * @param statusLine HTTP request status line
     * @return the version of the HTTP protocol
     * @throws IllegalArgumentException if unknown HTTP version
     */
    public static HttpVersion extractVersion(String statusLine) throws IllegalArgumentException {
        Matcher m = Pattern.compile(Http.HTTP + "/(\\d+)\\.(\\d+)").matcher(statusLine);
        if (m.find()) {
            if ((Integer.parseInt(m.group(1)) == 1) && (Integer.parseInt(m.group(2)) == 1)) {
                return VERSION_1_1;
            } else if ((Integer.parseInt(m.group(1)) == 1) && (Integer.parseInt(m.group(2)) == 0)) {
                return VERSION_1_0;
            } else {
                throw new IllegalArgumentException("Unknown HTTP Version");
            }
        } else {
            throw new IllegalArgumentException("Unknown HTTP Version");
        }
    }
}
