package com.iquest.yaws.http;

/**
 * Handle HTTP responses
 * After receiving and interpreting a request message, a server responds with an HTTP response message
 *
 * @see <a href="http://tools.ietf.org/html/rfc2616.html#section-6">RFC 2616 Section 6</a>
 */
public class HttpResponse extends HttpMessage {

    private HttpStatusCode statusCode;

    public HttpResponse(HttpRequest request) {

    }

    public HttpResponse() {
		// TODO Auto-generated constructor stub
	}

	public void getResponse() {

    }

    public HttpStatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(HttpStatusCode newStatusCode) {
    	statusCode = newStatusCode;
    }
    
    private void generateHeader() {

    }

    private void generateBody() {

    }
}
