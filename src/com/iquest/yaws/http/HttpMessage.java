package com.iquest.yaws.http;

import java.util.Map;

/**
 * @author Alex Butum
 * @see <a href="https://tools.ietf.org/html/rfc2616.html#page-31">RFC 2616 Section 4</a>
 */
public class HttpMessage {
    private HttpVersion httpVersion;
    private Map<String, String> headers;
    private byte[] body;

    public HttpVersion getHttpVersion() {
        return httpVersion;
    }

    public void setHttpVersion(HttpVersion version) {
        this.httpVersion = version;
    }

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }       
}
