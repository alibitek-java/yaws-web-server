package com.iquest.yaws.http;

/**
 * Available HTTP methods.
 *
 * @author Alex Butum
 * @see <a href="http://tools.ietf.org/html/rfc2616.html#section-5.1">RFC 2616 Section 5.1</a>
 */
public enum HttpMethod {
    HEAD,
    GET,
    POST,
    PUT,
    DELETE,
    TRACE,
    CONNECT,
    PATCH,
    OPTIONS;

    @Override
    public String toString() {
        return this.name();
    }

    /**
     * Extracts the HTTP method from the header status line
     *
     * @param statusLine HTTP request header status line
     * @return the HTTP method
     * @throws IllegalArgumentException
     */
    public static HttpMethod extractMethod(String statusLine) throws IllegalArgumentException {
        String method = statusLine.split(" ")[0];
        if (method != null) {
            return HttpMethod.valueOf(method);
        } else {
            throw new IllegalArgumentException();
        }
    }
}
