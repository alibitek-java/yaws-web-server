package com.iquest.yaws.server;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * HTTP server implementation using a multihreaded model with a new thread per client request
 * <p/>
 * Listens on a particular port number and address for incoming HTTP requests
 *
 * @author Alex Butum
 */
public class HttpServer extends Thread {
    public static final int DEFAULT_PORT = 8080;
    public static final String SERVER_NAME = "YAWS HTTP Server";
    public static final String SERVER_VERSION = "0.1";
    public static final String SERVER_SIGNATURE = SERVER_NAME + "/" + SERVER_VERSION;
    private int port;

    /**
     * Create a new HttpServer bound to the default port
     */
    public HttpServer() {
        this(DEFAULT_PORT);
    }

    /**
     * Create a new HttpServer bound to the given port.
     *
     * @param newPort listening port
     */
    public HttpServer(int newPort) {
        port = newPort;
    }

    /**
     * Create a new HttpServer using the configuration specified in the given XML file
     *
     * @param xmlFile the configuration file
     */
    public HttpServer(File xmlFile) {

    }

    @Override
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            int numberOfClients = 1;

            System.err.println("Listening on " + serverSocket.getInetAddress().getHostAddress() + ":"
                    + serverSocket.getLocalPort());
            System.err.println("Waiting for incoming connections from clients");

            while (true) {
                Socket client = serverSocket.accept();
                //client.setSoTimeout(20000);
                System.out.println("Spawning " + numberOfClients);
                Runnable r = new HttpHandler(client);
                Thread t = new Thread(r);
                t.start();
                numberOfClients++;
            }
        } catch (IOException e) {
            System.err.println("Exception thrown " + e.getMessage());
        } finally {
            System.out.println("Server Connection Closed");
        }
    }

    public int getPort() {
        return port;
    }
}
