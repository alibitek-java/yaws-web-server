package com.iquest.yaws.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    public static void main(String[] args) {
        //HttpServer yaws = new HttpServer(9000);
        HttpExecutorServer yaws = new HttpExecutorServer();
        yaws.start();
        Logger logger = LoggerFactory.getLogger(Main.class);
        logger.info("Hello World {} {}", HttpExecutorServer.SERVER_NAME, "ww");
    }
}
