package com.iquest.yaws.server;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * HTTP server implementation using a thread pool model
 * </p>
 * Listens on a particular port number and address for incoming HTTP requests.
 *
 * @author Alex Butum
 */
public class HttpExecutorServer {

    public static final String SERVER_NAME = "YAWS HTTP Server";

    public static final String SERVER_VERSION = "0.1";

    public static final int DEFAULT_PORT = 8080;

    public static final String SERVER_SIGNATURE = SERVER_NAME + "/" + SERVER_VERSION;

    private boolean running = false;

    private final ExecutorService handlerPool;

    private final ExecutorService dispatcherService;

    private final ServerSocket serverSocket;

    /**
     * Creates a new HTTP server.
     */
    public HttpExecutorServer() {
        this(DEFAULT_PORT);
    }

    /**
     * Creates a new HTTP server bound to the given port.
     *
     * @param port listening port
     */
    public HttpExecutorServer(int port) {
        try {
            serverSocket = new ServerSocket(port);
            handlerPool = Executors.newFixedThreadPool(16);
            dispatcherService = Executors.newSingleThreadExecutor();
        } catch (IOException e) {
            throw new RuntimeException("Error while starting server", e);
        }
    }

    public void dispatchRequest(Socket socket) {
        handlerPool.submit(new HttpExecutorHandler(socket, this));
    }

    public void start() {
        running = true;

        dispatcherService.submit(new Runnable() {
            @Override
            public void run() {
                while (running) {
                    try {
                        Socket socket = serverSocket.accept();
                        dispatchRequest(socket);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        System.out.println("Server started on port " + serverSocket.getLocalPort() + "...");
    }

    public void stop() {
        try {
            running = false;
            dispatcherService.shutdown();
            handlerPool.shutdown();
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Server stopped.");
        }
    }
}
