package com.iquest.yaws.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import com.iquest.yaws.http.HttpRequest;
import com.iquest.yaws.http.HttpResponse;

/**
 * Handle incoming client requests
 *
 * @author Alex Butum
 */
public class HttpHandler implements Runnable {

    private final Socket client;

    /**
     * Constructs a handler
     *
     * @param client the incoming client's socket connection
     */
    public HttpHandler(Socket client) {
        this.client = client;
    }

    @Override
    public void run() {
        try {
            InputStream is = client.getInputStream();
            OutputStream os = client.getOutputStream();

            HttpRequest request = new HttpRequest(is);
            HttpResponse response = new HttpResponse(request);

            // TODO: 

        } catch (IOException ex) {
            System.err.println("Exception thrown " + ex.getMessage());
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
