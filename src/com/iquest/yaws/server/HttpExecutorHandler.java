package com.iquest.yaws.server;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import com.iquest.yaws.http.Http;
import com.iquest.yaws.http.HttpMethod;
import com.iquest.yaws.http.HttpRequest;
import com.iquest.yaws.http.HttpResponse;
import com.iquest.yaws.http.HttpStatusCode;
import com.iquest.yaws.http.HttpVersion;

/**
 * 
 * Handle incoming client requests
 * 
 * @author Alex Butum
 */
public class HttpExecutorHandler implements Callable<Void> {
	private final Socket clientSocket;
	private final HttpExecutorServer server;

	/**
	 * Creates a new worker that handles the incoming request.
	 * 
	 * @param clientSocket the client socket this request is sent over.
	 * @param server a reference to the core server instance.
	 */
	public HttpExecutorHandler(Socket clientSocket, HttpExecutorServer server) {
		this.clientSocket = clientSocket;
		this.server = server;
	}

	@Override
	public Void call() throws Exception {
		// Parse request and prepare it from InputStream
		HttpRequest request = parseRequest(clientSocket.getInputStream());

		// Create appropriate response
		HttpResponse response = handleRequest(request);

		// Send response and close connection, if necessary
		if (keepAlive(request, response)) {
			sendResponse(response, clientSocket.getOutputStream());
			server.dispatchRequest(clientSocket);
		} else {
			response.getHeaders().put("Connection", "close");
			sendResponse(response, clientSocket.getOutputStream());
			clientSocket.close();
		}

		return null;
	}

	/**
	 * A helper method that reads an InputStream until it finds a CRLF (\r\n\).
	 * Everything in front of the linefeed occured is returned as String.
	 * 
	 * @param inputStream The stream to read from.
	 * @return The character sequence in front of the linefeed.
	 * @throws java.io.IOException
	 */
	private String readLine(InputStream inputStream) throws IOException {
		StringBuffer result = new StringBuffer();
		boolean crRead = false;
		int n;
		while ((n = inputStream.read()) != -1) {
			if (n == '\r') {
				crRead = true;
				continue;
			} else if (n == '\n' && crRead) {
				return result.toString();
			} else {
				result.append((char) n);
			}
		}
		return result.toString();
	}

	/**
	 * Parses an incoming HttpRequest. 
	 * </p>
	 * Reads the {@link java.io.InputStream} and creates an corresponding {@link javawebserver.http.HttpRequest} object, which will be returned.
	 * 
	 * @param inputStream the stream to read from.
	 * @return the request
	 * @throws java.io.IOException
	 */
	private HttpRequest parseRequest(InputStream inputStream) throws IOException {
		String firstLine = readLine(inputStream);

		HttpRequest request = new HttpRequest(inputStream);

		request.setHttpVersion(HttpVersion.extractVersion(firstLine));
		request.setRequestUri(firstLine.split(" ", 3)[1]);
		request.setHttpMethod(HttpMethod.extractMethod(firstLine));

		//String requestUri = firstLine.split(" ", 3)[1];
		//String[] requestParameters = requestUri.split("=");

		Map<String, String> headers = new HashMap<>();

		String nextLine = "";
		while (!(nextLine = readLine(inputStream)).equals("")) {
			String values[] = nextLine.split(":", 2);
			headers.put(values[0], values[1].trim());
		}

		request.setHeaders(headers);

		if (headers.containsKey(Http.CONTENT_LENGTH)) {
			int size = Integer.parseInt(headers.get(Http.CONTENT_LENGTH));
			byte[] data = new byte[size];
			int n;
			for (int i = 0; i < size && (n = inputStream.read()) != -1; i++) {
				data[i] = (byte) n;
			}
			request.setBody(data);
		} else {
			request.setBody(null);
		}

		return request;
	}

	/**
	 * Creates an appropriate {@link com.iquest.yaws.http.HttpResponse} to the
	 * given {@link com.iquest.yaws.http.HttpRequest}. 
	 * 
	 * </p>This method is not yet sending the response, just prepares it.
	 * 
	 * @param request The {@link com.iquest.yaws.http.HttpRequest} that must be handled.
	 * @return HttpResponse
	 */
	private HttpResponse handleRequest(HttpRequest request) {
		HttpResponse response = new HttpResponse();
		response.setHeaders(new HashMap<String, String>());
		response.getHeaders().put(Http.SERVER, HttpExecutorServer.SERVER_SIGNATURE);
		response.setHttpVersion(request.getHttpVersion());

		String requestUri = request.getRequestUri();
		if (requestUri.equals("/")) {
			requestUri = "/index.html";
		}

		// TODO: Replace absolute path with a relative path, relative to the project root
		File f = new File("/home/alex/Projects/SpringSourceToolSuite/yaws/webroot/" + requestUri);

		File rootDir = new File("/home/alex/Projects/SpringSourceToolSuite/yaws/webroot");
		try {
			if (!f.getCanonicalPath().startsWith(rootDir.getCanonicalPath())) {
				response.setStatusCode(HttpStatusCode.FORBIDDEN);
				return response;
			}
		} catch (IOException e1) {
			response.setStatusCode(HttpStatusCode.INTERNAL_SERVER_ERROR);
			return response;
		}

		if (f.exists()) {
			response.setStatusCode(HttpStatusCode.OK);
			InputStream inputStream;
			try {
				inputStream = new FileInputStream(f);
				byte fileContent[] = new byte[(int) f.length()];
				inputStream.read(fileContent);
				inputStream.close();
				response.setBody(fileContent);

				// guess and set the content type
				response.getHeaders().put(Http.CONTENT_TYPE, 
										  URLConnection.guessContentTypeFromName(f.getAbsolutePath()));
			} catch (FileNotFoundException e) {
				response.setStatusCode(HttpStatusCode.NOT_FOUND);
			} catch (IOException e) {
				response.setStatusCode(HttpStatusCode.INTERNAL_SERVER_ERROR);
			}
		} else {
			response.setStatusCode(HttpStatusCode.NOT_FOUND);
		}

		return response;
	}

	/**
	 * Sends a given {@link com.iquest.yaws.http.HttpResponse} over the given {@link java.io.OutputStream}
	 * 
	 * @param response
	 * @param outputStream
	 * @throws java.io.IOException
	 */
	public void sendResponse(HttpResponse response, OutputStream outputStream) throws IOException {
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));

		writer.write(response.getHttpVersion().toString());
		writer.write(' ');
		writer.write("" + response.getStatusCode().getStatusCode());
		writer.write(' ');
		writer.write(response.getStatusCode().getReasonPhrase());
		writer.write(Http.CRLF);

		if (response.getBody() != null && response.getBody().length > 0) {
			response.getHeaders().put(Http.CONTENT_LENGTH, "" + response.getBody().length);
		} else {
			response.getHeaders().put(Http.CONTENT_LENGTH, "" + 0);
		}

		if (response.getHeaders() != null) {
			for (String key : response.getHeaders().keySet()) {
				writer.write(key + ": " + response.getHeaders().get(key) + Http.CRLF);
			}
		}
		
		writer.write(Http.CRLF);
		writer.flush();

		if (response.getBody() != null && response.getBody().length > 0) {
			outputStream.write(response.getBody());
		}
		
		outputStream.flush();
	}

	/**
	 * Determines whether a connection should be kept alive or not on
	 * server-side. 
	 * </p>
	 * This decision is made based upon the given (
	 * {@link com.iquest.yaws.http.HttpRequest},
	 * {@link com.iquest.yaws.http.HttpResponse}) couple, respectively their header values. 
	 * </p>
	 * http://www.w3.org/Protocols/rfc2616/rfc2616-sec8.html
	 * http://en.wikipedia.org/wiki/HTTP_persistent_connection
	 * 
	 * @param request
	 * @param response
	 * @return true, if the server should keep open the connection, otherwise false.
	 */
	public boolean keepAlive(HttpRequest request, HttpResponse response) {
		if (response.getHeaders().containsKey(Http.CONNECTION) && response.getHeaders().get(Http.CONNECTION).equalsIgnoreCase("close")) {
			return false;
		}

		if (request.getHttpVersion().equals(HttpVersion.VERSION_1_0)) {
			if (request.getHeaders().containsKey(Http.CONNECTION) && request.getHeaders().get(Http.CONNECTION).equalsIgnoreCase("close")) {
				return false;
			} else {
				return true;
			}
		}

		return false;
	}
}
